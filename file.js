// First create a const fs to store the value fs, refering the readfileSync()
// second the variable tofixed to store the value of toFixed method which convert
//a number into string.

const fs = require('fs');
var toFixed = require('tofixed');

//A array variable coins to store the array values of types of coins in pairs
//of keys and values, name and value.
var coins = [{
    name: "10 pence",
    value: 0.1
  }, {
    name: "50 pence",
    value: 0.5
  }, {
    name: "1 pound",
    value: 1
  }, {
    name: "2 pound",
    value: 2
  }, {
    name: "5 pound",
    value: 5
  }];

  //A array variable products to store the array values of types of coins in pairs
  //of keys and values, name, type and cost.
var products = [{
    name: "Coke",
    type: "cold",
    cost: 3.0,
  }, {
    name: "Sprite",
    type: "cold",
    cost: 2.5
  }, {
    name: "Tea",
    type: "hot",
    cost: 1.5
  }, {
    name: "Coffee",
    type: "hot",
    cost: 2.1
  }];

// checkFile function to use try and catch statement to readfiles on node using fs
// and parsed the string store in the variable, with help of json methods.
checkFile = () => {
    try {
        var noteString = fs.readFileSync('machineMoney.json', null,JSON.stringify(0));  //to be able to use on command line fs.readFileSync().
        return JSON.parse(noteString);
    } catch (e) {
        console.log('No file');
        console.log('Creating new file for money in the machine');
        fs.writeFileSync('machineMoney.json', JSON.stringify(0));
    }
};

// variable created to use json stringfy() method for the calculation.
var savedAmount = (sum) => {
    fs.writeFileSync('machineMoney.json', JSON.stringify(sum));
}

// variable to store a function() of type of product, loop over and output their names and cost.
var productList = (productKind) => {
    console.log('------------------------------------------');
    console.log(' ',   productKind + '          beverages list:         |');
    console.log('------------------------------------------');
    for (let i = 0; i < products.length; i++) {                     //looping across the vairable product lenght
        if (products[i].type === productKind) {
            console.log(products[i].name, '£' + products[i].cost, '|');
            console.log('-------------');
            console.log('---------------------------------------------------------------------');
            console.log('Please choose your beverage  !! Do not Insert Coins Again!!         |');
            console.log('---------------------------------------------------------------------');

        }
    }
    // end checkFile() function;
};

// variable to store the function responsible to add coins check an return the total added
//number and string using json parsed.
var insertCoin = (coin) => {
    if (isNaN(coin) === true) { //if insert coin return value.
        return;
    }
    if (checkCoin(coin)) { // if checked using checkCoin() function and return
        return;
    } else { // otherwise
        var product = JSON.parse(fs.readFileSync('machineMoney.json')); //json method parsed()
        result = +coin + +product; //variable to store the coin added together parrsed variable.
        console.log('Current money in the machine:',  '£' + toFixed(result, 1));  //output result round 1 decimal place
        savedAmount(toFixed(result, 1)); //variable to store the result value at 1 decimal place.
        return JSON.parse(result);
    }
};//end of function insertCoin.

var checkCoin = (insertedCoin) => {    //declare variable to hold function value checkCoin() with parameter insertCoin
    let coin = coins.filter(key => key.value === parseFloat(insertedCoin)); // use parsed float inside filter() function to parsed the float values from array coins.
    var x = insertedCoin;  //declare a variable to store the value of insertCoin parameter.

    if(coin.length !=0) { // if statement to check coins length and output their values.
        if (x>=1) {
            console.log('-------------------------------');
            console.log('|', '£'+ x , 'Inserted to the machine! |'); // if added integer number output
            console.log('-------------------------------');

        }
        else {
            console.log('------------------------------------');
            console.log('p', x + ' Inserted to the machine! |'); // if added float number output
            console.log('------------------------------------');

        }
        return false; //boolean method if wrong number typed
    } else {   //otherwise add write numbers coins.
        console.log('|----------------!!Warning!!---------------|');
        console.log('|       This is not a valid coin           |');
        console.log('|Please enter 0.1 / 0.5 / 1 / 2 / 5 pounds |');
        console.log('--------------------------------------------');
        return true; // boolean true for message outputed.
    }
  }

var cancellation = (val) => { //declare variable to store cancellation() function for the code if everything output corretly return the message bellow.
    if (val>0) {
      console.log('Please take your drink', '\n');
      console.log('*==================================*');
      console.log('*  thank you for your purchased :) *');
      console.log('*==================================*');

    }
}

var calculateChange = (price) => {    //declare variable to store function calculateChange.
    var change = fs.readFileSync('machineMoney.json');  // variable to store readFileSync() method
    var currentChange = JSON.parse(change); // variable to parse the change to string using json() method.
    console.log('--------------------------------------');
    console.log('Current money in the machine: ',  '£' + toFixed(currentChange,1)+' ', '|');  //output the total value added rounded to 1 decimal place using toFixed()
    console.log('--------------------------------------');

// using if statement to check if change is correct and output the right message
    if (currentChange === toFixed(price, 1)) {
        console.log('No change!');
        cancellation(price);
        currentChange = 0;
    } else if (currentChange > price) {  // if change is greater than price return the rest.
        currentChange = currentChange - price;  // variable to store calculation change and price
        cancellation(price);
        console.log('----------------------------');
        console.log('Your change is: ', '£' + toFixed(currentChange,1), '|');   //output change rounded to 1 decimal place
        console.log('----------------------------');
        currentChange = 0;
    } else {
        console.log('------------------------------------------------');
        console.log('|       Please insert more money               |');
        console.log('------------------------------------------------');

    }
    savedAmount(currentChange); //sum of the current change using savedAmount() method.
}

// declare a variable to store the function of quantity of sugar on hot drinks.
var sugarAmount = (method, command, sugar) => {  //hold 3 parameters.
    let cost = products.filter(key => key.name === command).map(a => a.cost); // filter()mehtod on products array variable to get keys of name and cost.
    if(sugar!=null && sugar>=0 && sugar<6) {                                  // if statement to create a level of sugar from 1 to 5.
        console.log('The command amount is', command);
        console.log('--------------------------------------------------------');
        console.log('Orderd: ' + command + ' with ' + sugar + ' unit(s) sugar');
        console.log('--------------------------------------------------------');
        if (method == 'coins') {
            calculateChange(cost); // if purchased was used coins return function calculateChange()
        }
        if (method == 'credit') {
            creditCharge(cost);                               //if purchased was used credit card return function creditCharge()
            console.log('-------------------------');
            console.log('You will be charged: ', '£' + cost,  '|');
            console.log('-------------------------');
        }
    } else {                                                         //otherwise choose the amount of sugar again.
        console.log('------------------------------------------');
        console.log('| Please choose the amount of sugar 0-5  |');
        console.log('------------------------------------------');
    }
}

async function creditCharge(val) {    //declare a credit card charge function async to return a promise waiting for 5 seconds
    await sleep(5000); // after 5 seconds the value will be outputed.
    console.log('-------------------------------------------');
    console.log('Credit card has been charged ' + '£' + val, '\n'); // after 5 seconds the output credit card charge value
    console.log('Please take your drink');
    console.log('-------------------------------------------', '\n');

    console.log('*==================================*');
    console.log('*  thank you for your purchased :) *');
    console.log('*==================================*');


}

let sleep = (ms) => {  // function sleep for used on async above.
    return new Promise(resolve => setTimeout(resolve, ms));
  }

// now use the export modules to export all my functions below to main vending
//machine file 'Vm.js file'. with that there is not need to create a extension of the file.
module.exports = {
    productList,
    insertCoin,
    calculateChange,
    sugarAmount,
    creditCharge,
    checkFile
};
