// declare a const file to store the value require() function to be able to get import the functions() from file.js
const file = require('./file.js');

 //variables to store values responsible to pass a argument via command line using the array argv(argument values')
 // and follow the array from the 2 index.
var payment = process.argv[2];
var kind = process.argv[3];
var command = process.argv[4];
var amount = process.argv[5];

// output the vending machine panel  choices of pay, product or cancel
console.log('*=================================================================*');
console.log('||                    * Vending Machine *                        ||');
console.log('*=================================================================*');
console.log('-------------------------------------------------------------------');
console.log('|       Please choose paying method: credit or coins              |');
console.log('-------------------------------------------------------------------');
console.log('-------------------------------------------------------------------');
console.log('|  coins accept ||  10p : 50p : £1.00 : £2.00 : £5.00             |');
console.log('-------------------------------------------------------------------');
console.log('*=================================================================*');
console.log('|     Cold drinks       |   |       Hot drinks        |           |');
console.log('*=================================================================*');
console.log('| Coke £3 : Sprite £2.5 |   | Coffee £2.1 : Tea £1.5  |           |');
console.log('*=================================================================*');
console.log('|   You can cancel the operation at any state by add: Cancel      |');
console.log('*=================================================================*');


file.checkFile();           //using the import function checkFile() from file.js
if (payment === 'coins') {      // if statement to the user choose form of payment by coins of cold  and hot drinks and output messages
    console.log('-----------------------------------------------------------------------');
    console.log('| Please choose which type of drink you would like to buy (cold/hot) |');
    console.log('-----------------------------------------------------------------------');

    if (kind === 'cold') {  //if statement the user chooses cold drinks and outputs
        console.log('-------------------------------------------------------------------');
        console.log('|              First please Insert coins                          |');
        console.log('-------------------------------------------------------------------');
        file.insertCoin(command);                   //insertCoin() function imported from file.js with paremeter command.
        console.log(file.productList(kind));     // productList() function imported from file.js with parameter kind
        if (command === 'Coke')         {       //if statement to compare and calculate products value and change.
            console.log('Coke has selected');
            console.log('The price is: £3');
            file.calculateChange(3.0);            // calculateChange() function imported from file.js with parameter of products Coke cost
        } else if (command === 'Sprite')         {
            console.log('Sprite has selected');
            console.log('The price is: £2.5');
            file.calculateChange(2.5);             // calculateChange() function imported from file.js with parameter of products Sprite cost
        } else if (command === 'Cancel') {
            console.log('Action canceled');
            file.calculateChange(0);             // calculateChange() function imported from file.js with parameter of product cancel value 0.
        }
    }

    else if (kind === 'hot') {                  //if else statement the user chooses hot drinks and outputs
        console.log('Insert coins');
        file.insertCoin(command);               //insertCoin() function imported from file.js with paremeter command.
        console.log(file.productList(kind));     // productList() function imported from file.js with parameter kind
        if (command === 'Coffee') {                 //if user orde Coffee output this message and function.
            console.log('Coffee has selected');
            console.log('The price is: £2.1');
            file.sugarAmount(payment, command, amount);      // sugarAmount() function imported from file.js with 3 parameters
            if (amount === 'Cancel') {
                console.log(' Action canceled!');
                file.calculateChange(0);                    // calculateChange() function imported from file.js with parameter of product cancel value 0.
            }
        } else if (command === 'Tea') {        //if user orde Tea output this message and function
            console.log('Tea has selected');
            console.log('The price is: £1.5');
            file.sugarAmount(payment, command, amount);
            if (amount === 'Cancel') {
                console.log(' Action canceled!');
                file.calculateChange(0);                 // calculateChange() function imported from file.js with parameter of product cancel value 0.
            }
        } else if (command === 'Cancel') {
            console.log(' Action canceled!');
            file.calculateChange(0);                     // calculateChange() function imported from file.js with parameter of product cancel value 0.
        }
    }

    else if (kind === 'Cancel') {        //otherwise output this message plus function.
        console.log(' Action canceled!');
        file.calculateChange(0);
    }
}

   // if user choose pay by credit card
else if (payment === 'credit') {
    console.log('------------------------------------------------------------------------');
    console.log('| Please choose which type of drink you would like to buy (cold/hot)  |');
    console.log('------------------------------------------------------------------------');
    if (kind == 'cold') {                  //if statement the user chooses cold drinks and outputs
       console.log('-------------------------------------------------------------------');
       console.log('|                Please choose beverage      :D                   |');
       console.log('-------------------------------------------------------------------');
        console.log(file.productList(kind));           // productList() function imported from file.js with parameter kind
        if (command === 'Coke') {                       //if statement to compare and calculate products value and change.
            console.log('Coke has selected');
            console.log('You will be charged: £3');
            file.creditCharge(3);                          // calculateChange() function imported from file.js with parameter of products Coke cost
        } else if (command === 'Sprite') {
            console.log('Sprite has selected');
            console.log('You will be charged: £2.5');
            file.creditCharge(2.5);                        // calculateChange() function imported from file.js with parameter of products Sprite cost
        }
    }

   //if else statement the user chooses hot drinks and outputs
     else if (kind == 'hot') {
        console.log('-------------------------------------------------------------------');
        console.log('|                Please choose beverage         :D                |');
        console.log('-------------------------------------------------------------------');
        console.log(file.productList(kind));                     // productList() function imported from file.js with parameter kind
        if (command === 'Coffee') {                              //if user orde Coffee output this message and function.
            console.log('Coffee has selected');
            file.sugarAmount(payment, command, amount);          // sugarAmount() function imported from file.js with 3 parameters
        } else if (command === 'Tea') {
            console.log('Tea has selected');                       //if user orde Tea output this message and function
            file.sugarAmount(payment, command, amount);
        } else if (command === 'Cancel') {
            console.log(' Action canceled!');
            file.calculateChange(0);                                  // calculateChange() function imported from file.js with parameter of product cancel value 0.
        }
    } else if (kind === 'Cancel') {                               //otherwise output this message plus function.
        console.log(' Action canceled!');
        file.calculateChange(0);
    }
}
