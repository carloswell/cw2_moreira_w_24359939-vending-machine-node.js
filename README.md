        How to use Vending machine on node.js
        
        
                Delivered by:
              Moreira_W_24359939:

       Module Leader: Dr MUHAMMAD AWAIS
          Email: awaism@edgehill.ac.uk 





1- introduction
This document is a basic instruction detail of how to use the vending machine application, that will give some step by step instructions of how
to use the application on the command line with the help of node.js to run any javaScript file on the command line computer, as has been asked on coursework2.
The instruction must be readed and followed all parts  carefully before the attempt to run the application. This instruction will be subdivided in 3  parts: 
important information, how to run and examples of application. 
This document instruction  will be also available in the report body of the cw2. 

2- important information:

*This application was built with 2 js files, vm.js and file.js. It needs both files to be able to work. 

*Before the user starts to run the file, it should install two external libraries in his command line: tofixed and readline-sync.

*There is not a user interface (IU)  for this application, the script runs straight on the command line.

*The file takes the argument that the user inserts on the command line and returns output.

*The user can cancel the process at any time by type Cancel on the command line.

*The file works with node.js version 8 and above.

*The file creates a json file in the folder to save data from the application.


2.1 - How to run:

*Open any command line on the PC  that works function node.js.

*Install the library toFixed typing on the command line: “ npm i tofixed --save”. 

*Install the library readline-sync typing on the command line: “ npm install readline-sync”.

*Type on the command line the file name “vm.js” to start the application.

*Read through the application panel and follow the instructions .

*First action type  again vm.js followed by payment choice, credit or coins  “e.g. vm.js coins”.

*Second, the user needs to choose which type of drink, cold or hot “e.g. vm.js coins hot”.

*After the user chooses the type of drink, the application will display a list of drinks cold or hot.

*The user can choose the drink from the list, type the name of the drink or how many coins he wants to insert if choose coins at the beginning, “e.g. vm.js coins hot 5”.

*After the user types how many coins he wants to insert he can choose the drink he wants.

*If the user didn’t type  not enough coins the application will ask more coins if it is over the application will provide the drink and change will be retrieved to the user.

*If the user choosed hot drinks the user will have an extra option to choose how many units of sugar he wants, in a range of “0 to 5 units of sugar”, “e.g. vm.js coins hot Coffee 2”.


2.2 - Examples:

Starting the application on the command line.

(bash command line $ type node vm.js)

1st action: choose payment.

(vending machine screen choose payment method)

(bash command line $ type node vm.js coins)

2nd action: type of drink.
		
(vending machine screen choose type of drink)

(bash command line $ type node vm.js coins hot)

3rd the list of drinks displayed insert coins.

(screen display hot beverage list and price, Tea, coffee)

(bash command line $ type node vm.js coins hot 5)

5th adding coins “e.g. 5” choose the drink.

(screen display hot beverage list and price, and current money inserted)

(bash command line $ type node vm.js coins hot coffee)

6th choose the option amount of sugar.

(screen display  amount of sugar to choose between  0-5)


(bash command line $ type node vm.js coins hot Coffee with “Capital letter on first letter of Coffee or in any other product” )


7th order displaying and changing if  coins added were greater than.

(screen display  ordered done description)

(screen display  retrieved the change back for of £2.9)
